# OpenFOAM Tutorials

This is a set of tutorials for using OpenFOAM developed by students of the Centre for Doctoral Training in Fluid Dynamics at the University of Leeds.
6 different tutorials have been developed covering the following topics:

1. **Introduction to OpenFOAM**: This will tell you how to get access to OpenFOAM on University of Leeds Linux machines and give a quick overlook of OpenFOAM.

2. **Cavity**: Here you will go over how a case is set up in OpenFOAM by running a simple benchmark problem.

3. **Pitz Daily**: This will show how to use turbulence models in OpenFOAM based on the Pitz-Daily backwards facing step problem.

4. **Meshing**: This will cover everything you need to know about creating and converting meshes in OpenFOAM, by covering blockMesh and snappyHexMesh, as well as as conversion utilities like fluentMeshToFoam.

5. **Solution Improvement**: In this tutorial you will learn how to get better results by choosing appropriate numerical schemes and solution methods.

6. **Customising IcoFOAM**: In this tutorial you will learn how to customise a solver to your own needs.


## Getting started

To get started clone over this folder onto your personal machine:

```
git clone https://gitlab.com/uol_openfoam_resources/openfoam-tutorials.git
```


## Authors and acknowledgment
Original Authors: C. Lloyd, E. Greiciunas, A. Oates, E. Harvey, T. Sykes, R. Hetherington

Modified by : J. Frank, Y. Darbar


## Disclaimer
This tutorial has been prepared by PhD students from the EPSRC Centre for Doctoral Training in Fluid Dynamics, University of Leeds. Although the contents of each tutorial is checked, this is not a peer-reviewed document. As such, we cannot guarantee it is completely error-free.
