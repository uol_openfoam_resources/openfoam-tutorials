By the end of the session, you will know:
- How Openfoam is used, some benefits and background to the software.
- How to set up OpenFOAM and some useful shortcut commands.
- The general procedure for generating and working through a problem on OpenFOAM.
